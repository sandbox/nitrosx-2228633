<?php
/**
 * @file
 * custom module that create a custom block where the variable 
 * site version is exposed.
 * It allows to customize the label prepended to the variable and 
 * additional formatted text can be added below
 *
 * resources:
 * @link http://fourkitchens.com/blog/2012/07/18/building-custom-blocks-drupal-7
 */

/**
 * Implements hook_help().
 */
function site_version_help($path, $arg) {
  switch ($path) {
    case "admin/help#site_version":

      return '<p>' . t("Displays site version in a custom block") . '</p>';

  }
}


/**
 * Implements hook_block_info().
 */
function site_version_block_info() {
  $blocks = array();
  $blocks['site_version_block'] = array(
    'info' => t('Site Version Block'),
  );
  return $blocks;
}


/**
 * Implements hook_block_configure().
 */
function site_version_block_configure($delta = '') {
  $form = array();

  switch ($delta) {
    case 'site_version_block':
      // Text field with label that is going to be prepended to version string.
      $form['side_version_label'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Site version label'),
        '#default_value' => variable_get('site_version_label', 'Version'),
      );

      // Text field with version string.
      $form['side_version'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Site version'),
        '#default_value' => variable_get('site_version', 'Unknown'),
      );

      // Additional text to be shown with version.
      $form['site_version_extra'] = array(
        '#type'          => 'text_format',
        '#title'         => t('Additional text that needs to be shown together within the version block'),
        '#default_value' => variable_get('site_version_extra', ''),
      );

      break;
  }
  return $form;
}


/**
 * Implements hook_block_save().
 */
function site_version_block_save($delta = '', $edit = array()) {
  switch ($delta) {
    case 'site_version_block':
      // Saving site version label.
      variable_set('site_version_label', $edit['side_version_label']);

      // Saving site version.
      variable_set('site_version', $edit['side_version']);

      // Saving the WYSIWYG text.
      variable_set('site_version_extra', $edit['site_version_extra']['value']);

      break;
  }
}


/**
 * Implements hook_block_view().
 */
function site_version_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'site_version_block':

      $block['subject'] = variable_get('site_version_title', '');
      $block['content'] = _site_version_view();

      break;
  }

  return $block;
}

/**
 * Returns a renderable array of the content block.
 *
 * Custom function to assemble renderable array for block content.
 * Returns a renderable array with the block content.
 *
 * @return array
 *   returns a renderable array of block content.
 */
function _site_version_view() {

  // Create block content.
  $content = array(
    'site_version' => array(
      '#prefix' => '<div id="site_version_block" class="site_version_block">',
      '#suffix' => '</div>',
      'version' => array(
        '#prefix' => '<div id="site_version" class="site_version">',
        '#suffix' => '</div>',
        'label' => array(
          '#prefix' => '<span id="site_version_label" class="site_version_label">',
          '#type'   => 'markup',
          '#markup' => variable_get('site_version_label', 'Version') . " : ",
          '#suffix' => '</span>',
        ),
        'value' => array(
          '#prefix' => '<span id="site_version_value" class="site_version_value">',
          '#type'   => 'markup',
          '#markup' => variable_get('site_version', 'Unknown'),
          '#suffix' => '</span>',
        ),
      ),
    ),
  );

  // Get site version extra.
  $extra = variable_get('site_version_extra', '');
  // Add extra only if we have any.
  if ($extra) {
    // Add extra text.
    $content['site_version']['extra'] = array(
      '#prefix' => '<div id="site_version_extra" class="site_version_extra">',
      '#type'   => 'markup',
      '#markup' => $extra,
      '#suffix' => '</div>',
    );
  }

  return $content;
}
